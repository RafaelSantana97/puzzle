# puzzle

Quebra-Cabeça 3x3, 4x4, 5x5, 6x6, 7x7, 8x8

De complexidade (n²)!/2, quando resolvido sem heurística

https://github.com/GBelchior contribuiu com parte da heurística do Projeto, 
tornando possível que quebra-cabeças NxN com N < 7 sejam resolvidos instantaneamente
e um quebra-cabeça com N = 7 seja resolvido em 5 segundos, em média.