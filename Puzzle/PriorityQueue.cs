﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Puzzle
{
    public class PriorityQueue<T, K> where K : class
    {
        private Dictionary<T, ConcurrentQueue<K>> queue;

        public int Count { get; set; }

        public PriorityQueue()
        {
            queue = new Dictionary<T, ConcurrentQueue<K>>();
            Count = 0;
        }

        public void Enqueue(T key1, K v)
        {
            if (!queue.ContainsKey(key1))
                queue.Add(key1, new ConcurrentQueue<K>());
            queue[key1].Enqueue(v);
            Count++;
        }

        public K Dequeue()
        {
            if (queue.Keys.Count == 0)
                return null;
            T minKey1 = queue.Keys.Min();
            K v;
            queue[minKey1].TryDequeue(out v);
            if (queue[minKey1].Count == 0)
                queue.Remove(minKey1);
            Count--;
            return v;
        }
    }
}