using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Threading;

namespace Puzzle
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class Puzzle : Form
    {
        private Button b00;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private Button[,] BL;
        private Button btnSolve;
        private TextBox txtResult;
        private Button bRand;
        private Button btnAnimate;
        private short[] table;
        private NumericUpDown nudSize;
        private Button btnResize;
        private short size = 7;

        private bool alreadySolved = true;
        private bool alreadyAnimated = true;

        public Puzzle()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            // Draws the painel
            DrawPanel();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            b00 = new Button();
            btnSolve = new Button();
            txtResult = new TextBox();
            bRand = new Button();
            btnAnimate = new Button();
            nudSize = new NumericUpDown();
            btnResize = new Button();
            ((System.ComponentModel.ISupportInitialize)nudSize).BeginInit();
            SuspendLayout();
            // 
            // b00
            // 
            b00.BackColor = System.Drawing.Color.White;
            b00.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            b00.Location = new System.Drawing.Point(8, 8);
            b00.Name = "b00";
            b00.Size = new System.Drawing.Size(56, 56);
            b00.TabIndex = 2;
            b00.Text = "X";
            b00.UseVisualStyleBackColor = false;
            b00.Visible = false;
            b00.Click += new EventHandler(b00_Click);
            // 
            // btnSolve
            // 
            btnSolve.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btnSolve.Location = new System.Drawing.Point(56 * size + 20, 10);
            btnSolve.Name = "btnSolve";
            btnSolve.Size = new System.Drawing.Size(200, 40);
            btnSolve.TabIndex = 3;
            btnSolve.Text = "Solução";
            btnSolve.Click += new System.EventHandler(btnSolve_Click);
            // 
            // txtResult
            // 
            txtResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            txtResult.Location = new System.Drawing.Point(10, Math.Max(56 * size + 20, 245));
            txtResult.Multiline = true;
            txtResult.Name = "txtResult";
            txtResult.ScrollBars = ScrollBars.Vertical;
            txtResult.Size = new System.Drawing.Size(56 * size + 210, 80 + 10 * size);
            txtResult.TabIndex = 4;
            // 
            // bRand
            // 
            bRand.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            bRand.Location = new System.Drawing.Point(56 * size + 20, 60);
            bRand.Name = "bRand";
            bRand.Size = new System.Drawing.Size(200, 40);
            bRand.TabIndex = 5;
            bRand.Text = "Sortear";
            bRand.Click += new EventHandler(bRand_Click);
            //
            // btnAnimate
            btnAnimate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btnAnimate.Location = new System.Drawing.Point(56 * size + 20, 110);
            btnAnimate.Name = "btnAnimate";
            btnAnimate.Size = new System.Drawing.Size(200, 40);
            btnAnimate.TabIndex = 5;
            btnAnimate.Text = "Animar";
            btnAnimate.Click += new EventHandler(btnAnimate_Click);
            //
            // 
            // nudSize
            // 
            nudSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            nudSize.Location = new System.Drawing.Point(56 * size + 20, 160);
            nudSize.Maximum = 9;
            nudSize.Minimum = 3;
            nudSize.Name = "nudSize";
            nudSize.Size = new System.Drawing.Size(200, 40);
            nudSize.TabIndex = 6;
            nudSize.Value = size;
            // 
            // btnResize
            // 
            btnResize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            btnResize.Location = new System.Drawing.Point(56 * size + 20, 195);
            btnResize.Name = "btnResize";
            btnResize.Size = new System.Drawing.Size(200, 40);
            btnResize.TabIndex = 7;
            btnResize.Text = "Redimensionar";
            btnResize.Click += new EventHandler(btnResize_Click);
            // 
            // Puzzle
            // 
            AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            ClientSize = new System.Drawing.Size(56 * size + 230, 90 + 10 * size + Math.Max(56 * size + 20, 245));
            Controls.Add(btnResize);
            Controls.Add(btnAnimate);
            Controls.Add(nudSize);
            Controls.Add(bRand);
            Controls.Add(txtResult);
            Controls.Add(btnSolve);
            Controls.Add(b00);
            Name = "Puzzle";
            Text = "Quebra Cabeça";
            ((System.ComponentModel.ISupportInitialize)nudSize).EndInit();
            ResumeLayout(false);
            PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new Puzzle());
        }

        /// <summary>
        /// Draws the game panel
        /// </summary>
        private void DrawPanel()
        {
            table = new short[size * size];
            for (short i = 0; i < size * size; i++)
            {
                table[i] = i;
            }
            //table = new int[] { 4, 2, 8, 0, 7, 3, 5, 6, 1 };

            // Creating the labels
            BL = new Button[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Button B = new Button
                    {
                        Parent = b00.Parent,
                        Font = b00.Font,
                        Size = b00.Size,
                        Left = i * 56 + 10,
                        Top = j * 56 + 10,
                        BackColor = b00.BackColor,
                        Text = (table[j * size + i] != 0 ? table[j * size + i].ToString() : ""),
                        Visible = true
                    };
                    B.Click += new System.EventHandler(b00_Click);
                    BL[i, j] = B;
                }
            }

        }


        /// <summary>
        /// Click template for all buttons 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void b00_Click(object sender, EventArgs e)
        {
            Button b = sender as Button;
            int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    // search for button
                    if (BL[i, j] == b)
                    {
                        // Move to clear position 
                        x2 = x1 = i;
                        y2 = y1 = j;
                        if ((i > 0) && (BL[i - 1, j].Text == ""))
                            x2--;
                        if ((i < size - 1) && (BL[i + 1, j].Text == ""))
                            x2++;
                        if ((j > 0) && (BL[i, j - 1].Text == ""))
                            y2--;
                        if ((j < size - 1) && (BL[i, j + 1].Text == ""))
                            y2++;
                    }
                }
            }
            // Chances clear with clicked
            string temp = BL[x1, y1].Text;
            BL[x1, y1].Text = BL[x2, y2].Text;
            BL[x2, y2].Text = temp;
        }


        /// <summary>
        /// Solves the puzzle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSolve_Click(object sender, EventArgs e)
        {
            if (alreadySolved)
            {
                return;
            }

            // First, update table structure with buttons configuration
            int k = 0;
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    table[k++] = (BL[j, i].Text == "" ? (short) 0 : Convert.ToInt16(BL[j, i].Text));

            // Now the agent looks for the parameter target
            short[] solution = new short[size * size];
            for (short i = 0; i < size * size; i++)
            {
                solution[i] = i;
            }

            // Clears memo
            txtResult.Text = "";
            EightPuzzle ComputerAgent = new EightPuzzle(table, solution, size);

            // Writes the solution to the tet box
            short[] res = ComputerAgent.GetSolution();
            ComputerAgent = null;
            GC.Collect();

            if (res == null)
                txtResult.Text += "Tamanho limite de busca alcan�ado.";
            else
            {
                foreach (int i in res)
                    txtResult.Text += i.ToString() + ",";
                txtResult.Text += "#";
                txtResult.Text = txtResult.Text.Replace(",#", "");
                txtResult.Text = txtResult.Text.Replace("#", "");
            }

            alreadySolved = true;
        }

        private int FindPosition(int value)
        {
            for (int i = 0; i < size * size; i++)
            {
                if (table[i] == value)
                    return i;
            }
            return -1;
        }

        List<int> GetNeighbours(int index)
        {
            List<int> neighbours = new List<int>();
            if (index - size > 0)
                neighbours.Add(index - size);
            if (index + size < size * size)
                neighbours.Add(index + size);
            if (index - 1 >= 0 && index / size == (index - 1) / size)
                neighbours.Add(index - 1);
            if (index + 1 < size * size && index / size == (index + 1) / size)
                neighbours.Add(index + 1);
            return neighbours;
        }

        /// <summary>
        /// Animate the solution
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnimate_Click(object sender, EventArgs e)
        {
            btnSolve_Click(sender, e);

            if (alreadyAnimated) return;

            string result = txtResult.Text;
            string[] steps = result.Split(',');
            int x2, x1, y2, y1;
            int timeInterval = (int)Math.Ceiling((double)Math.Min(steps.Length * 1000, 10000) / (double)steps.Length);
            foreach (string step in steps)
            {
                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < size; j++)
                    {
                        if (BL[i, j].Text == step.Trim())
                        {
                            x2 = x1 = i;
                            y2 = y1 = j;
                            if ((i > 0) && (BL[i - 1, j].Text == ""))
                                x2--;
                            if ((i < size - 1) && (BL[i + 1, j].Text == ""))
                                x2++;
                            if ((j > 0) && (BL[i, j - 1].Text == ""))
                                y2--;
                            if ((j < size - 1) && (BL[i, j + 1].Text == ""))
                                y2++;
                            string temp = BL[x1, y1].Text;
                            BL[x1, y1].Text = BL[x2, y2].Text;
                            BL[x2, y2].Text = temp;
                            i = j = size;
                        }
                    }
                }
                Update();
                Thread.Sleep(timeInterval);
            }

            alreadyAnimated = true;
        }

        /// <summary>
        /// Random configure the table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bRand_Click(object sender, EventArgs e)
        {
            int current = FindPosition(0);
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < 1000 * size * size; i++)
            {
                List<int> neighbours = GetNeighbours(current);
                int randomNeighbour = neighbours[r.Next(neighbours.Count)];
                short temp = table[randomNeighbour];
                table[randomNeighbour] = table[current];
                table[current] = temp;
                current = randomNeighbour;
            }
            int k = 0;
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    BL[j, i].Text = (table[k] == 0 ? "" : table[k].ToString());
                    k++;
                }

            txtResult.Text = "";
            alreadySolved = false;
            alreadyAnimated = false;
        }

        private void btnResize_Click(object sender, EventArgs e)
        {
            short aux = Convert.ToInt16(nudSize.Value);
            if (aux == size) return;

            size = aux;
            Controls.Clear();
            InitializeComponent();
            DrawPanel();
            alreadySolved = true;
            alreadyAnimated = true;
        }
    }
}