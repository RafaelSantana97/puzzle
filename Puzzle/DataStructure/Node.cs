﻿using System;
using System.Collections.Generic;

namespace ProjetoGrafos.DataStructure
{
    /// <summary>
    /// Classe que representa um nó dentro de um grafo.
    /// </summary>
    public class Node
    {
        #region Propriedades

        /// <summary>
        /// O nome do nó dentro do grafo.
        /// </summary>
        private int HashCode { get; set; }
        private bool IsHashCodeComputed;
        /// <summary>
        /// A informação adicional armazenada no nó.
        /// </summary>
        public short[] Bloco { get; set; }
        /// <summary>
        /// A lista de arcos associada ao nó.
        /// </summary>
        public List<Edge> Edges { get; private set; }

        public int Nivel { get; set; }

        #endregion

        #region Construtores

        /// <summary>
        /// Cria um novo nó.
        /// </summary>
        public Node()
        {
            Edges = new List<Edge>();
        }

        /// <summary>
        /// Cria um novo nó.
        /// </summary>
        /// <param name="hashcode">O nome do nó.</param>
        /// <param name="bloco">A informação armazenada no nó.</param>
        public Node(short[] bloco, int nivel) : this()
        {
            Bloco = bloco;
            Nivel = nivel;
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Adiciona um arco com nó origem igual ao nó atual, e destino e custo de acordo com os parâmetros.
        /// </summary>
        /// <param name="to">O nó destino.</param>
        /// <param name="cost">O custo associado ao arco.</param>
        public void AddEdge(Node to, short cost)
        {
            Edges.Add(new Edge(this, to, cost));
        }

        #endregion

        #region Métodos Sobrescritos

        /// <summary>
        /// Apresenta a visualização do objeto em formato texto.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return HashCode.ToString();
        }

        public override int GetHashCode()
        {
            if (IsHashCodeComputed) return HashCode;

            HashCode = Bloco.Length;
            for (int i = 0; i < Bloco.Length; ++i)
            {
                HashCode = unchecked(HashCode * 314159 + Bloco[i]);
            }

            IsHashCodeComputed = true;
            return HashCode;
        }

        #endregion
    }
}