using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using ProjetoGrafos.DataStructure;

namespace Puzzle
{
    /// <summary>
    /// PuzzleAgent - searchs solution for the eight puzzle problem
    /// </summary>
    public class EightPuzzle : Graph
    {
        private readonly short lado;
        private readonly int lim;
        private readonly short[] initState;
        private readonly short[] target;

        private HashSet<int> caminhosEncontrados = new HashSet<int>();
        private PriorityQueue<int, Node> prioridade = new PriorityQueue<int, Node>();


        /// <summary>
        /// Creating the agent and setting the initialstate plus target
        /// </summary>
        /// <param name="InitialState"></param>
        public EightPuzzle(short[] InitialState, short[] Target, short size)
        {
            initState = InitialState;
            target = Target;
            lado = size;
            lim = size * size;
        }

        /// <summary>
        /// Accessor for the solution
        /// </summary>
        public short[] GetSolution()
        {
            return FindSolution();
        }

        /// <summary>
        /// Fun��o principal de busca
        /// </summary>
        /// <returns></returns>
        private short[] FindSolution()
        {
            Node noInicial = new Node(initState, 0);

            prioridade.Enqueue(FuncAvaliacao(noInicial), noInicial);
            caminhosEncontrados.Add(noInicial.GetHashCode());

            while (prioridade.Count > 0)
            {
                Node n = prioridade.Dequeue();

                if (TargetFound(n.Bloco))
                {
                    Debug.WriteLine("Buscado em "+ caminhosEncontrados.Count() + " caminhos");
                    return BuildAnswer(n);
                }
                GetSucessors(n);
            }

            return null;
        }


        private short GetEmpty(short[] bloco)
        {
            foreach (short i in bloco)
                if (bloco[i] == 0) return i;

            return 0;
        }

        private void GetSucessors(Node n)
        {
            short posicaoEmBranco = GetEmpty(n.Bloco);

            void Move(int novaPosicao)
            {
                short[] newSuccessor = new short[lim];
                Array.Copy(n.Bloco, newSuccessor, lim);

                newSuccessor[novaPosicao] = newSuccessor[posicaoEmBranco];
                newSuccessor[posicaoEmBranco] = n.Bloco[novaPosicao];

                Node aux = new Node(newSuccessor, n.Nivel + 1);

                if (!caminhosEncontrados.Contains(aux.GetHashCode()))
                {
                    aux.AddEdge(n, newSuccessor[posicaoEmBranco]);
                    caminhosEncontrados.Add(aux.GetHashCode());
                    prioridade.Enqueue(FuncAvaliacao(aux), aux);
                }
            }

            // Can Move Left
            if (posicaoEmBranco % lado != 0)
            {
                Move(posicaoEmBranco - 1);
            }
            // Can Move Right
            if (posicaoEmBranco % lado != lado - 1)
            {
                Move(posicaoEmBranco + 1);
            }
            // Can Move Up
            if ((posicaoEmBranco / lado) > 0)
            {
                Move(posicaoEmBranco - lado);
            }
            // Can Move Down
            if ((posicaoEmBranco / lado) < lado - 1)
            {
                Move(posicaoEmBranco + lado);
            }
        }

        private short[] BuildAnswer(Node n)
        {
            List<short> res = new List<short>();

            while (n.Edges.Count > 0)
            {
                res.Add(n.Edges[0].Cost);
                n = n.Edges[0].To;
            }

            res.Reverse();
            return res.ToArray();
        }

        private bool TargetFound(short[] bloco)
        {
            return bloco.SequenceEqual(target);
        }

        private int FuncAvaliacao(Node n)
        {
            int manhattam = 0;

            short[] auxConflitoLinear = new short[lim];
            int conflitoLinear = 0;

            for (short i = 0; i < lim; i++)
            {
                if (n.Bloco[i] == 0) continue;

                short j = n.Bloco[i];

                int auxManhattan = Math.Abs(i % lado - j % lado) + Math.Abs(i / lado - j / lado);

                if (auxManhattan == 1)
                {
                    auxConflitoLinear[i] = j;
                }

                manhattam += auxManhattan;
            }

            for (short i = 0; i < lim; i++)
            {
                if (auxConflitoLinear[i] == 0) continue;

                for (short j = 0; j < lim; j++)
                {
                    if (auxConflitoLinear[j] == 0) continue;
                    if (i == j) continue;
                    if (auxConflitoLinear[i] == j && auxConflitoLinear[j] == i)
                    {
                        conflitoLinear++;
                        break;
                    }
                }
            }

            return (4 * conflitoLinear + 3 * manhattam) * lim + (lado) * n.Nivel;
        }
    }
}